﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tobii.Gaming;

public abstract class UI_Clickable : MonoBehaviour
{
	public RectTransform trans;
	public static List<UI_Clickable> visibleUI = new List<UI_Clickable>();

	void Awake()
	{
		trans = gameObject.GetComponent<RectTransform>();
		visibleUI.Add(this);
	}

	void OnDisable()
	{
		visibleUI.Remove(this);
	}

	// Update is called once per frame
	void Update ()
	{
		
	}

	/// <summary>
	/// When clickable UI element is gazed upon enter this function.
	/// </summary>
	public abstract void OnClickEnter();
}
