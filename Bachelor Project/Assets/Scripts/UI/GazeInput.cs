﻿using Tobii.Gaming;
using UnityEngine;

public class GazeInput : MonoBehaviour
{
	public static GazeInput instance = null;

	private void Start()
	{
		TobiiAPI.SubscribeGazePointData();
	}

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else
		{
			Destroy(gameObject);
		}
		DontDestroyOnLoad(this);
	}

	// Update is called once per frame
	private void Update()
	{
		if (UI_Clickable.visibleUI != null)
		{ 
			GazePoint gazePoint = TobiiAPI.GetGazePoint();
			UI_Clickable temp;

			float gazeX, gazeY;
			gazeX = gazePoint.Screen.x;
			gazeY = gazePoint.Screen.y;
			for (int i = 0; i < UI_Clickable.visibleUI.Count; ++i)
			{
				temp = UI_Clickable.visibleUI[i];
				if (temp.trans.rect.Contains(new Vector2(gazeX, gazeY)))
				{
					temp.OnClickEnter();
				}
			}
		}
	}
}