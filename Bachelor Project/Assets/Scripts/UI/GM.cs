﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GM : MonoBehaviour
{
	public static GM instance = null;

	//Program Settings
	public float clickTimer = 1;
	public int fontSize = 14;

	//Flags
	public bool pauseable = true;

	// Use this for initialization
	void Start ()
	{
		if (instance == null)
		{
			instance = this;
		}
		else if (instance != this)
		{
			Destroy(gameObject);
			return;
		}
	}
}