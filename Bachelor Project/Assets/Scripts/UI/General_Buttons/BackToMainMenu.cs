﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackToMainMenu : UI_Clickable
{
	public override void OnClickEnter()
	{
		SceneLoader.instance.LoadScene(0);
	}
}
