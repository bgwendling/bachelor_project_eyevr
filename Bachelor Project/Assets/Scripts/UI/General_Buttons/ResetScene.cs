﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetScene : UI_Clickable
{ 
	public override void OnClickEnter()
	{
		SceneLoader.instance.ResetScene();
	}
}
