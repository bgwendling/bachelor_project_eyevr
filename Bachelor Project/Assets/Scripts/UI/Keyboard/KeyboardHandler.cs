﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This is the keyboard class, it will handle creating, displaying, and functionality of the keyboard.
/// </summary>
public class KeyboardHandler : MonoBehaviour
{
	public static KeyboardHandler instance = null;

	public float distanceFromCenter = 50;

	[SerializeField]
	private GameObject keyboardButton;

	private string lowerCase = "abcdefghijklmnopqrstuvwxyz";
	private string upperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private string symbols = "<>()[]!\"@#£$€%&+-*/=?,;.:'"; //this one probably needs editing lol

	private GameObject lowerCaseKeyboard, upperCaseKeyboard, symbolsKeyboard;

	private string currentEdit = null;

	void Start()
	{
		if (instance == null)
		{
			instance = this;
		}
		else if (instance != this)
		{
			Destroy(gameObject);
			return;
		}

		List<GameObject> keyboards = new List<GameObject>();
		lowerCaseKeyboard = Instantiate(new GameObject("Lower case keyboard"), this.transform);
		upperCaseKeyboard = Instantiate(new GameObject("Upper case keyboard"), this.transform);
		symbolsKeyboard = Instantiate(new GameObject("Symbols keyboard"), this.transform);
		keyboards.Add(lowerCaseKeyboard);
		keyboards.Add(upperCaseKeyboard);
		keyboards.Add(symbolsKeyboard);
		List<string> keyboardStrings = new List<string>();
		keyboardStrings.Add(lowerCase);
		keyboardStrings.Add(upperCase);
		keyboardStrings.Add(symbols);


		float angle;
		KeyboardButton temp;
		Vector3 position = new Vector3();

		for (int i = 0; i < 3; i++)
		{
			angle = 0;
			for (int j = 0; j < keyboardStrings[i].Length; j++)
			{
				angle = j * Mathf.PI * 2 / keyboardStrings[i].Length;
				position.x = Mathf.Cos(angle) * distanceFromCenter;
				position.y = Mathf.Sin(angle) * distanceFromCenter;
				position.z = 0;

				temp = Instantiate(keyboardButton, keyboards[i].transform, false).GetComponent<KeyboardButton>();
				position += gameObject.transform.position;
				temp.transform.position = position;
				temp.Initialize(keyboardStrings[i][j]);
			}
		}

		//lowerCaseKeyboard.SetActive(false);
		upperCaseKeyboard.SetActive(false);
		symbolsKeyboard.SetActive(false);
	}

	void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else if (instance != this)
		{
			Destroy(gameObject);
			return;
		}
	}

	void Update()
	{

	}

	/// <summary>
	/// Toggles keyboard on or off
	/// </summary>
	public void ToggleKeyboard()
	{
		lowerCaseKeyboard.SetActive(false);
		upperCaseKeyboard.SetActive(false);
		symbolsKeyboard.SetActive(false);
	}

	/// <summary>
	/// Adds a string to the string currently being edited. ( ͡° ͜ʖ ͡°)
	/// </summary>
	/// <param name="key">The string to be added</param>
	public void Write(string key)
	{
		if (currentEdit != null)
		{
			currentEdit += key;
		}
	}

	public void SetString(string newstring)
	{
		currentEdit = newstring;
	}
}