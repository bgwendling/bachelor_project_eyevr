﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using Tobii.Gaming;
public class KeyboardButton : UI_Clickable {
	private char character = ' ';

	public override void OnClickEnter()
	{
		KeyboardHandler.instance.Write(character.ToString());
	}

	public void Initialize(char character)
	{
		this.character = character;
		GetComponentInChildren<Text>().text = character.ToString();
	}
}
