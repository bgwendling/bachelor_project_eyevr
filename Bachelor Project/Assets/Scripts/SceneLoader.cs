﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

/// <summary>
/// Purpose of this class is to have the ability to load different scenes, and easily add in support for carrying over variables to the new scene
/// </summary>
public class SceneLoader : MonoBehaviour
{
	public static SceneLoader instance = null;

	//Initializing
	void Awake ()
	{
		if(instance == null)
		{
			instance = this;
		}
		else if(instance != this)
		{
			Destroy(gameObject);
			return;
		}
		DontDestroyOnLoad(gameObject);
	}

	/// <summary>
	/// Loads a new scene by scene index (as listed in unity build settings)
	/// </summary>
	/// <param name="sceneIndex">Index of Scene to be loaded</param>
	public void LoadScene(int sceneIndex)
	{
		SceneManager.LoadScene(sceneIndex);
	}

	/// <summary>
	/// Loads a new scene by scene name
	/// </summary>
	/// <param name="sceneIndex">Name of Scene to be loaded</param>
	public void LoadScene(string sceneName)
	{
		SceneManager.LoadScene(sceneName);
	}

	/// <summary>
	/// Reloads the currently active scene.
	/// </summary>
	public void ResetScene()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}
}
